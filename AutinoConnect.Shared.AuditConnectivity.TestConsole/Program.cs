﻿using AutinoConnect.Shared.AudiConnectivity.Model;
using AutinoConnect.Shared.AuditConnectivity.Interfaces;
using System;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using AutinoConnect.Shared.AuditConnectivity.Concrete;
using System.Linq;
using System.Collections.Generic;

namespace AutinoConnect.Shared.AuditConnectivity.TestConsole
{
    class Program 
    {

        static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IAuditService, AuditService>()
                .BuildServiceProvider();



            var Auditlogger = serviceProvider.GetService<IAuditService>();
            var AuditLog = new AuditModel {
                Info = new InfoModel {
                    AuditType = "FINANCE",
                    CorrelationId =  Guid.NewGuid().ToString()
                }, 
                AuditLog = new AuditLogModel { 
                    Product = "TEST" 
                } 
            };
        
            var cancellationToken = new CancellationToken();

             Auditlogger.CreateAudit(AuditLog, cancellationToken);

                   


            Console.WriteLine("logz log entry submitted sent");
            Console.ReadKey();
        }

       
    }
}
