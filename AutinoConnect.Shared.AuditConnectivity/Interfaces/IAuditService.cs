﻿using AutinoConnect.Shared.AudiConnectivity.Model;
using System.Threading;
using System.Threading.Tasks;

namespace AutinoConnect.Shared.AuditConnectivity.Interfaces
{
    public interface IAuditService
    {
        Task<string> CreateAudit(object content, CancellationToken cancellationToken);
        
    }
}