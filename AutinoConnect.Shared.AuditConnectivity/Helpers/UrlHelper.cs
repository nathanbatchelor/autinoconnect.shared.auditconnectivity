﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutinoConnect.Shared.AuditConnectivity.Helpers
{
    public class UrlHelper
    {
        public static string CombineUrl<K, V>(string baseurl, Dictionary<K, V> uriParams)
        {
            char[] trims = new char[] { '\\', '/' };
            string uri = string.Format("{0}{1}", baseurl, "?");
            if (uriParams != null && uriParams.Any())
            {
                foreach (var uriParam in uriParams)
                {
                    uri += string.Format("{0}={1}{2}", (uriParam.Key.ToString() ?? string.Empty).TrimEnd(trims), (uriParam.Value.ToString() ?? string.Empty).TrimEnd(trims), "&");
                }
            }
            return uri;
        }
    }
}
