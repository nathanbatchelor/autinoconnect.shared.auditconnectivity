﻿using Newtonsoft.Json;

namespace AutinoConnect.Shared.AudiConnectivity.Model
{
    public class AuditModel
    {
        public InfoModel Info { get; set; }
        public AuditLogModel AuditLog { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}