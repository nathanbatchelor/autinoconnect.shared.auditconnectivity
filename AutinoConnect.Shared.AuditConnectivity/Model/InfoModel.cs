﻿using Newtonsoft.Json;

namespace AutinoConnect.Shared.AudiConnectivity.Model
{
    public class InfoModel
    {
        public InfoModel() { }
        public InfoModel(string auditType, string auditRetentionDays)
        {
            AuditType = auditRetentionDays;
            AuditRetentionDays = auditRetentionDays;
        }
        public string AuditType { get; set; }
        public string AuditRetentionDays { get; set; }
        public string AuditDateTime { get; set; }
        public string ClientId { get; set; }
        public string ClientIp { get; set; }
        public string CorrelationId { get; set; }

    }
}