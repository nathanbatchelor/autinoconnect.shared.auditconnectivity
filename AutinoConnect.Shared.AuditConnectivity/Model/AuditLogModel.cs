﻿using Newtonsoft.Json;

namespace AutinoConnect.Shared.AudiConnectivity.Model
{
    public class AuditLogModel
    {
        public string Application { get; set; }
        public string Product { get; set; }
        public string URL { get; set; }
        public string Environment { get; set; }
        public string IsCached { get; set; }
        public string ResponseCode { get; set; }
        public string UserAction { get; set; }
        public string UserType { get; set; }
        public string User { get; set; }
        public string Action { get; set; }
        public string Version { get; set; }
        public string Payload { get; set; }

    }
}