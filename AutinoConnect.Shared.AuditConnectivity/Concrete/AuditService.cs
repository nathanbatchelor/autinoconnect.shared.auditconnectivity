﻿using AutinoConnect.Shared.AuditConnectivity.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutinoConnect.Shared.AuditConnectivity.Concrete
{
    public class AuditService : IAuditService
    {
        private HttpClient client = null;

        public AuditService()
        {
            client = new HttpClient();
        }

        static string Url = ConfigurationManager.AppSettings["AuditUrl"];
        static string Token = ConfigurationManager.AppSettings["AuditToken"];
        static string Type = ConfigurationManager.AppSettings["AuditType"];

        public async Task<string> CreateAudit(object content, CancellationToken cancellationToken)
        {
            try
            {
                Dictionary<string, string> UrlParams = new Dictionary<string, string>
                {
                    { "token", Token },
                    { "type", Type }
                };
                var AuditServiceUrl = Helpers.UrlHelper.CombineUrl(Url, UrlParams);
                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Post, AuditServiceUrl))
                using (var httpContent = CreateHttpContent(content))
                {
                    request.Content = httpContent;

                    using (var response = await client
                        .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken)
                        .ConfigureAwait(false))
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Second exception caught.", e);
            }

            return "";
        }
        private static HttpContent CreateHttpContent(object content)
        {
            HttpContent httpContent = null;

            if (content != null)
            {
                var ms = new MemoryStream();
                SerializeJsonIntoStream(content, ms);
                ms.Seek(0, SeekOrigin.Begin);
                httpContent = new StreamContent(ms);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }

            return httpContent;
        }
        public static void SerializeJsonIntoStream(object value, Stream stream)
        {
            using (var sw = new StreamWriter(stream, new UTF8Encoding(false), 1024, true))
            using (var jtw = new JsonTextWriter(sw) { Formatting = Formatting.None })
            {
                var js = new JsonSerializer();
                js.Serialize(jtw, value);
                jtw.Flush();
            }
        }

       
    }
}